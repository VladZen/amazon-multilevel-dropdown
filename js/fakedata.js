var fakeData = [{
	"id": "children per woman",
	"units": ""
},{
	"id": "CO2 emissions",
	"units": "tonnes per person"
},{
"id": "Income per person",
"children": [{
   "id": "war_and_peace",
   "children": [
      {"id": "milit_exp", "units": "dollars"},
      {"id": "battl_deaths", "units": "dollars"}
   ]
},{
   "id": "dem_score",
   "children": [
      {"id": "test", "units": "dollars"},
      {"id": "ts", "units": "dollars"}
   ]
}]
},{
"id": "Child mortality",
"children": [{
   "id": "nutrition",
   "children": [
      {"id": "malnutr", "units": "dollars"},
      {"id": "sugar_per_cap", "units": "kg"},
      {"id": "total_food", "units": "kg"}
   ]
},{
   "id": "Life expectancy",
   "children": [
      {"id": "suicide_absolute", "units": ""},
      {
         "id": "suicide_relative",
         "children": [
            {"id": "testing", "units": "dollars"},
            {"id": "tststs", "units": "dollars"}
         ]
      }
   ]
}],
"special": true

},{
   "id": "Infrastructure",
   "children": [
      {"id": "Water", "units": "dollars"},
      {"id": "Communication",
      "children": [
         {"id": "Broadband subscribers (total)", "units": "dollars"},
         {"id": "Broadband subscribers (per 100 people)", "units": "kg"},
         {"id": "Cell phones (total)",
         "children": [
            {"id": "Nokia", "units": "dollars"},
            {"id": "iPhone", "units": "kg"},
            {"id": "Samsung", "units": "kg"},
            {"id": "Sony", "units": "kg"},
            {"id": "Vertu",
               "children": [
               {"id": "Nokia", "units": "dollars"},
               {"id": "iPhone", "units": "kg"},
               {"id": "Samsung", "units": "kg"},
               {"id": "Sony", "units": "kg"},
               {"id": "Vertu", "units": "kg"},
               {"id": "Sagem", "units": "kg"},
               {"id": "Ericsson",
               "children": [
               {"id": "Nokia", "units": "dollars"},
               {"id": "iPhone", "units": "kg"},
               {"id": "Samsung", "units": "kg"},
               {"id": "Sony", "units": "kg"},
               {"id": "Vertu", "units": "kg"},
               {"id": "Sagem", "units": "kg"},
               {"id": "Ericsson",
               "children": [
               {"id": "Nokia", "units": "dollars"},
               {"id": "iPhone", "units": "kg"},
               {"id": "Samsung", "units": "kg"},
               {"id": "Sony", "units": "kg"},
               {"id": "Vertu", "units": "kg"},
               {"id": "Sagem", "units": "kg"},
               {"id": "Ericsson", "units": "kg"}
            ]}
            ]}
            ],
            },
            {"id": "Sagem", "units": "kg"},
            {"id": "Ericsson", "units": "kg"}
         ],
         "special": true},
         {"id": "Cell phones (per 100 people)", "units": "kg"},
         {"id": "Internet users (total)", "units": "dollars"},
         {"id": "Internet users (per 100 people)", "units": "kg"},
         {"id": "Personal computers (total)", "units": "dollars"},
         {"id": "Personal computers (per 100 people)", "units": "kg"}
      ]},
      {"id": "Traffic", "units": "kg"},
      {"id": "Sanitation", "units": "kg"},
      {"id": "Indicators", "units": "kg"}
   ]
},{
   "id": "Energy",
   "units": "tonnes per person"
},{
   "id": "Work",
   "children": [
      {"id": "Employment rate",
      "children": [
         {"id": "Aged 15-24 employment rate", "units": "dollars"},
         {"id": "Aged 15+ employment rate", "units": "kg"},
         {"id": "Females 15-24 employment rate", "units": "dollars"},
         {"id": "Females 15+ employment rate", "units": "kg"},
         {"id": "Males 15-24 employment rate", "units": "dollars"},
         {"id": "Males 15+ employment rate", "units": "kg"},
      ]},
      {"id": "Unemployement", "units": "kg"},
      {"id": "Employment by status", "units": "kg"},
      {"id": "Indicators", "units": "kg"},
      {"id": "Employment by sector", "units": "kg"}
   ]
},{
   "id": "Environment",
   "units": "tonnes per person"
},{
   "id": "Society",
   "units": "years"
},{
   "id": "Health",
   "units": "tonnes per person"
}

];

var data = [{
   "id": "properties",
"children": [{
      "id": "geo",
"unit": "",
"use": "property",
"scales": ["ordinal"]
},{
   "id": "geo.region",
"unit": "",
"use": "property",
"scales": ["ordinal"]
}]
},{
   "id": "indicators",
"children": [{

   "id": "lex",
   "unit": "years",
   "use": "indicator",
"scales": ["linear"]
},{
   "id": "gdp_per_cap",
   "unit": "$/year/person",
   "use": "indicator",
"scales": ["log","linear"]
},{
   "id": "pop",
   "unit": "",
   "use": "indicator",
"scales": ["linear","log"]
}]
},{
   "id": "time",
   "unit": "year",
   "use": "indicator",
"scales": ["time"]
}];

var translator = {
   "en":{
      "geo": "Country",
      "geo.region": "World region",
      "lex": "Life expectancy",
      "gdp_per_cap": "GDP per capita",
      "pop": "Population",
      "time": "Time"
   },
   "se":{
      "geo": "Land",
      "geo.region": "Världsregion",
      "lex": "Livslängd",
      "gdp_per_cap": "BNP per capita",
      "pop": "Befolkning",
      "time": "Tid"
   }
};